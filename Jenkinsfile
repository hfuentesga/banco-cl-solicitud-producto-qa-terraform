pipeline {
  agent {
    docker {
      image 'registry.fif.tech/fif-terraform'
      label 'azure-slave'
      args '-u root --entrypoint="" -v /var/lib/root/.ssh:/root/.ssh'
    }
  }
  environment {
    ARM_SUBSCRIPTION_ID=credentials('ephimeral_node_subscription_id')
    ARM_CLIENT_ID=credentials('ephimeral_node_client_id')
    ARM_CLIENT_SECRET=credentials('ephimeral_node_client_secret')
    ARM_TENANT_ID=credentials('ephimeral_node_tenant_id')
    TF_VAR_path_fif_validator=credentials('TF_VAR_path_fif_validator')
    VAULT_ADDR = 'https://vault.fif.tech:8200'
    VAULT_SKIP_VERIFY = 'true'
  }
  stages {
    stage('checkout repo') {
      steps {
        script {
          def scmvars = checkout scm
          env.GIT_BRANCH = scmvars.GIT_BRANCH
          env.GIT_COMMIT = scmvars.GIT_COMMIT
          env.GIT_PREVIOUS_COMMIT = scmvars.GIT_PREVIOUS_COMMIT
          env.GIT_PREVIOUS_SUCCESSFUL_COMMIT = scmvars.GIT_PREVIOUS_SUCCESSFUL_COMMIT
          env.GIT_URL = scmvars.GIT_URL

          env.EMPTY_COMMIT = (env.GIT_PREVIOUS_COMMIT != 'null' && env.GIT_PREVIOUS_SUCCESSFUL_COMMIT == env.GIT_COMMIT)

          if(env.BUILD_CAUSE_UPSTREAMTRIGGER == null){
            env.BUILD_CAUSE_UPSTREAMTRIGGER = 'false'
          }
        }
        bitbucketStatusNotify ( buildState: 'INPROGRESS' )
      }
    }
    stage('terraform fmt') {
      steps {
        script {
          sh  '''
            terraform fmt -check=true -diff=true
          '''
        }
      }
    }
    stage('terraform init') {
      steps {
        withCredentials([azureStorage(credentialsId: 'TFSTATE_ACCESS_KEY')]) {
          script {
            sh  '''
              terraform init -no-color -backend=true -input=false -backend-config="access_key=$AZURE_STORAGE_ACCOUNT_KEY"
            '''
          }
        }
      }
    }
    stage('terraform import') {
      steps {
        withCredentials([azureStorage(credentialsId: 'TFSTATE_ACCESS_KEY'), usernamePassword(credentialsId: 'CLOUDFLARE_AUTH', usernameVariable: 'CLOUDFLARE_EMAIL', passwordVariable: 'CLOUDFLARE_TOKEN'),string(credentialsId: 'swarm-cluster-role-id', variable: 'ROLE_ID'), string(credentialsId: 'swarm-cluster-secret-id', variable: 'SECRET_ID')]) {
          script {
            sh  '''
              set +x
              export VAULT_TOKEN=\$(vault write -field=token auth/approle/login role_id=${ROLE_ID} secret_id=${SECRET_ID})
              if [ $(terraform state list module.vnet-fif-terraform.azurerm_resource_group.fif | wc -l) -eq 0 ]; then
                env VAULT_TOKEN=$VAULT_TOKEN terraform import module.vnet-fif-terraform.azurerm_resource_group.fif /subscriptions/7884d3f2-56b4-4ef6-8c6b-f0d3ae8ad186/resourcegroups/Fiftech
              fi

              if [ $(terraform state list module.vnet-fif-terraform.azurerm_virtual_network.fif | wc -l) -eq 0 ];then
                terraform import module.vnet-fif-terraform.azurerm_virtual_network.fif /subscriptions/7884d3f2-56b4-4ef6-8c6b-f0d3ae8ad186/resourceGroups/Fiftech/providers/Microsoft.Network/virtualNetworks/VNET01_EAST2_FIF
              fi
              set -x
            '''
          }
        }
      }
    }
    stage('terraform validate') {
      steps {
        withCredentials([azureStorage(credentialsId: 'TFSTATE_ACCESS_KEY'), usernamePassword(credentialsId: 'CLOUDFLARE_AUTH', usernameVariable: 'CLOUDFLARE_EMAIL', passwordVariable: 'CLOUDFLARE_TOKEN')]) {
          script {
            sh  """
              terraform validate -no-color
            """
          }
        }
      }
    }
    stage('terraform plan') {
      steps {
        withCredentials([azureStorage(credentialsId: 'TFSTATE_ACCESS_KEY'), usernamePassword(credentialsId: 'CLOUDFLARE_AUTH', usernameVariable: 'CLOUDFLARE_EMAIL', passwordVariable: 'CLOUDFLARE_TOKEN'), string(credentialsId: 'swarm-cluster-role-id', variable: 'ROLE_ID'), string(credentialsId: 'swarm-cluster-secret-id', variable: 'SECRET_ID')]) {
          script {
            sh  """
              set +x
              env VAULT_TOKEN=\$(vault write -field=token auth/approle/login role_id=${ROLE_ID} secret_id=${SECRET_ID}) terraform plan -no-color -out=tfplan -input=false
              set -x
            """
          }
        }
      }
    }
    stage('ask deploy') {
      when {
        allOf {
          branch 'master';
          environment name: 'BUILD_CAUSE_UPSTREAMTRIGGER', value: 'false'
          environment name: 'EMPTY_COMMIT', value: 'false'
        }
      }
      steps {
        script {
          timeout(time: 10, unit: 'MINUTES') {
            input(id: "Deploy Gate", message: "Deploy ${params.project_name}?", ok: 'Deploy')
          }
        }
      }
    }
    stage('apply') {
      when {
        allOf {
          branch 'master';
          environment name: 'BUILD_CAUSE_UPSTREAMTRIGGER', value: 'false'
          environment name: 'EMPTY_COMMIT', value: 'false'
        }
      }
      steps {
        withCredentials([azureStorage(credentialsId: 'TFSTATE_ACCESS_KEY'), string(credentialsId: 'swarm-cluster-role-id', variable: 'ROLE_ID'),string(credentialsId: 'swarm-cluster-secret-id', variable: 'SECRET_ID'), usernamePassword(credentialsId: 'CLOUDFLARE_AUTH', usernameVariable: 'CLOUDFLARE_EMAIL', passwordVariable: 'CLOUDFLARE_TOKEN')]) {
          script {
            sh  """
              set +x
              env VAULT_TOKEN=\$(vault write -field=token auth/approle/login role_id=${ROLE_ID} secret_id=${SECRET_ID}) terraform apply -no-color -input=false tfplan
              set -x
            """
          }
        }
      }
    }
    }

  post {
    always {
      cleanWs()
      deleteDir()
    }
    success {
      bitbucketStatusNotify ( buildState: 'SUCCESSFUL' )
      slackSend (channel: "#devops_notifications", color: "#00FF00", message: "${env.JOB_NAME} ${env.BUILD_DISPLAY_NAME} SUCCESSFUL after ${currentBuild.durationString.replace(' and counting', '')} (<${env.BUILD_URL}|Open>)")
    }
    failure {
      bitbucketStatusNotify ( buildState: 'FAILED' )
      sh 'env | sort'
      slackSend (channel: "#devops_notifications", color: "danger", message: "${env.JOB_NAME} ${env.BUILD_DISPLAY_NAME} FAILED after ${currentBuild.durationString.replace(' and counting', '')} (<${env.BUILD_URL}|Open>)")
    }
    aborted {
      bitbucketStatusNotify ( buildState: 'FAILED' )
      sh 'env | sort'
      slackSend (channel: "#devops_notifications", color: "grey", message: "${env.JOB_NAME} ${env.BUILD_DISPLAY_NAME} ABORTED after ${currentBuild.durationString.replace(' and counting', '')} (<${env.BUILD_URL}|Open>)")
    }
  }
}
