provider "cloudflare" {
  version = "1.7"
}

resource "cloudflare_record" "public_lb_ip1" {
  domain  = "${var.domain}"
  name    = "${replace(var.slug, "fif-", "")}-${var.environment}"
  proxied = true
  type    = "CNAME"
  value   = "${var.slug}-${var.environment}-ip1.eastus2.cloudapp.azure.com"
}

resource "cloudflare_record" "private_lb" {
  domain  = "${var.domain}"
  name    = "${replace(var.slug, "fif-", "")}-${var.environment}-priv"
  proxied = false
  type    = "A"
  value   = "${module.swarm.lb_private_ip_address}"
}
