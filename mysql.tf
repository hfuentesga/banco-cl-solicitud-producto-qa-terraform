# MYSQL Database provisioning, uncomment in case the cluster needs a MySQL database
# Don't forget to specify the mysql sizing below AND to do a Terraform FMT
# DON'T RUN IN THE FIRST PROVISIONING, IT CONFLICTS WITH THE TERRAFORM IMPORT OF VNET
# Migrate to mysql module when possible
resource "random_string" "mysql_admin_user" {
  length  = 8
  special = false
  number  = false
}

resource "random_string" "mysql_serviceuser_user" {
  length  = 8
  special = false
  number  = false
}

resource "random_string" "mysql_admin" {
  length           = 32
  special          = true
  override_special = "!*-_=+<>"
}

resource "random_string" "mysql_serviceuser" {
  length           = 32
  special          = true
  override_special = "!*-_=+<>"
}

resource "azurerm_mysql_server" "mysql-server" {
  name                = "${var.slug}-${var.environment}"
  location            = "${module.vnet-fif-terraform.location}"
  resource_group_name = "${module.swarm.resource_group_name}"

  #SPECIFY THE DATABASE SIZING!
  sku {
    name     = "GP_Gen5_2"
    capacity = 2
    tier     = "GeneralPurpose"
    family   = "Gen5"
  }

  storage_profile {
    storage_mb            = 25600
    backup_retention_days = 7
    geo_redundant_backup  = "Disabled"
    auto_grow             = "Enabled"
  }

  administrator_login          = "${random_string.mysql_admin_user.result}"
  administrator_login_password = "${random_string.mysql_admin.result}"
  version                      = "5.7"
  ssl_enforcement              = "Enabled"
}

resource "azurerm_mysql_database" "cluster-schema" {
  name                = "${replace(var.slug, "-", "_")}_${var.environment}"
  resource_group_name = "${module.swarm.resource_group_name}"
  server_name         = "${azurerm_mysql_server.mysql-server.name}"
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

resource "vault_generic_secret" "vault-mysql-admin" {
  path = "kv/clusters/${var.slug}-${var.environment}-cluster/priv/tools/mysql"

  data_json = <<EOT
 {
   "MYSQL_HOST": "${azurerm_mysql_server.mysql-server.fqdn}",
   "MYSQL_ADMIN_USER": "${random_string.mysql_admin_user.result}@${azurerm_mysql_server.mysql-server.name}",
   "MYSQL_ADMIN_PASSWORD": "${azurerm_mysql_server.mysql-server.administrator_login_password}"
 }
 EOT
}

resource "vault_generic_secret" "vault-mysql-serviceuser" {
  path = "kv/clusters/${var.slug}-${var.environment}-cluster/shared/automated/tools/mysql"

  data_json = <<EOT
 {
   "MYSQL_HOST": "${azurerm_mysql_server.mysql-server.fqdn}",
   "MYSQL_SERVICEUSER_USER": "${random_string.mysql_serviceuser_user.result}@${azurerm_mysql_server.mysql-server.name}",
   "MYSQL_SERVICEUSER_PASSWORD": "${random_string.mysql_serviceuser.result}"
 }
 EOT
}

resource "azurerm_mysql_firewall_rule" "azure" {
  name                = "${var.slug}-allow-azure-${var.environment}"
  resource_group_name = "${azurerm_mysql_server.mysql-server.resource_group_name}"
  server_name         = "${azurerm_mysql_server.mysql-server.name}"
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}

resource "azurerm_mysql_firewall_rule" "rundeck" {
  name                = "${var.slug}-allow-rundeck-${var.environment}"
  resource_group_name = "${azurerm_mysql_server.mysql-server.resource_group_name}"
  server_name         = "${azurerm_mysql_server.mysql-server.name}"
  start_ip_address    = "34.199.212.194"
  end_ip_address      = "34.199.212.194"
}

provider "mysql" {
  version  = "1.5.0"
  endpoint = "${azurerm_mysql_server.mysql-server.fqdn}"
  username = "${azurerm_mysql_server.mysql-server.administrator_login}@${azurerm_mysql_server.mysql-server.name}"
  password = "${azurerm_mysql_server.mysql-server.administrator_login_password}"
  tls      = "true"
}

resource "mysql_user" "schema-user" {
  user               = "${random_string.mysql_serviceuser_user.result}"
  host               = "%"
  plaintext_password = "${random_string.mysql_serviceuser.result}"
}

resource "mysql_grant" "schema-grants" {
  user       = "${mysql_user.schema-user.user}"
  host       = "${mysql_user.schema-user.host}"
  database   = "${azurerm_mysql_database.cluster-schema.name}"
  privileges = ["ALL"]
}

resource "azurerm_mysql_firewall_rule" "ligarius" {
  name                = "${var.slug}-allow-ligarius-${var.environment}"
  resource_group_name = "${azurerm_mysql_server.mysql-server.resource_group_name}"
  server_name         = "${azurerm_mysql_server.mysql-server.name}"
  start_ip_address    = "200.10.167.71"
  end_ip_address      = "200.10.167.71"
}
