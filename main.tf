provider "azurerm" {
  version = "1.40.0"
}

terraform {
  backend "azurerm" {
    container_name       = "tfstates"
    key                  = "banco-cl-solicitud-producto/qa/swarm_module.tfstate"
    resource_group_name  = "devops-ci"
    storage_account_name = "fiftfstatestorage"
  }

  required_version = ">= 0.11.14"
}

# vnet-fif module
module "vnet-fif-terraform" {
  source = "git::ssh://git@bitbucket.org/falabellafif/vnet-fif-terraform.git?ref=v1.1.0"
}
