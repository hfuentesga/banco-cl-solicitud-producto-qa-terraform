# resource "azurerm_network_security_rule" "kong" {
#   name                    = "Allow_kong"
#   priority                = "200"
#   direction               = "Inbound"
#   access                  = "Allow"
#   protocol                = "*"
#   source_port_range       = "*"
#   source_address_prefixes = ["190.153.235.117"] # Moneda 970
#
#   destination_port_ranges     = [8443]
#   destination_address_prefix  = "*"
#   resource_group_name         = "${module.swarm.resource_group_name}"
#   network_security_group_name = "${module.swarm.network_security_group_name}"
# }
#
resource "azurerm_network_security_rule" "kong" {
  name                        = "Allow_kong"
  priority                    = "4008"
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefixes     = ["10.145.72.48/28"]                           # Moneda 970
  destination_port_ranges     = [443, 8443]
  destination_address_prefix  = "*"
  resource_group_name         = "${module.swarm.resource_group_name}"
  network_security_group_name = "${module.swarm.network_security_group_name}"
}

resource "azurerm_network_security_rule" "allow_salesforce_to_kong" {
  name                    = "allow_salesforce_to_kong"
  priority                = "201"
  direction               = "Inbound"
  access                  = "Allow"
  protocol                = "*"
  source_port_range       = "*"
  destination_port_ranges = [443]

  #Rangos Ip's Salesforce Latinoamerica
  source_address_prefixes = [
    "13.108.0.0/14",
    "66.231.80.0/20",
    "68.232.192.0/20",
    "96.43.144.0/20",
    "128.17.0.0/16",
    "136.146.0.0/15",
    "198.245.80.0/20",
    "199.122.120.0/21",
    "204.14.232.0/21",
  ]

  destination_address_prefix  = "*"
  resource_group_name         = "${module.swarm.resource_group_name}"
  network_security_group_name = "${module.swarm.network_security_group_name}"
}

resource "azurerm_network_security_rule" "allow_omni_to_kong" {
  name                    = "allow_omni_to_kong"
  priority                = "204"
  direction               = "Inbound"
  access                  = "Allow"
  protocol                = "*"
  source_port_range       = "*"
  destination_port_ranges = [443, 8443]

  #Rangos Ip's banco-cl-omni-ms-qa
  source_address_prefixes = [
    "10.242.222.0/24",
    "10.232.222.0/24",
  ]

  destination_address_prefix  = "*"
  resource_group_name         = "${module.swarm.resource_group_name}"
  network_security_group_name = "${module.swarm.network_security_group_name}"
}

resource "azurerm_network_security_rule" "allow_financiamiento_to_kong" {
  name                    = "allow_financiamiento_to_kong"
  priority                = "205"
  direction               = "Inbound"
  access                  = "Allow"
  protocol                = "*"
  source_port_range       = "*"
  destination_port_ranges = [443, 8443]

  #Rangos Ip's fif-financiamiento-qa
  source_address_prefixes = [
    "10.145.65.144/28", # QA
    "10.145.65.112/28", # Test
  ]

  destination_address_prefix  = "*"
  resource_group_name         = "${module.swarm.resource_group_name}"
  network_security_group_name = "${module.swarm.network_security_group_name}"
}

resource "azurerm_network_security_rule" "allow_backendcl_to_kong" {
  name                    = "allow_backendcl_to_kong"
  priority                = "210"
  direction               = "Inbound"
  access                  = "Allow"
  protocol                = "*"
  source_port_range       = "*"
  destination_port_ranges = [443, 8443]

  #Rangos Ip's banco-cl-backend
  source_address_prefixes = [
    "172.20.241.92",
    "172.20.241.93",
    "172.20.241.94",
    "172.20.241.95",
  ]

  destination_address_prefix  = "*"
  resource_group_name         = "${module.swarm.resource_group_name}"
  network_security_group_name = "${module.swarm.network_security_group_name}"
}

resource "azurerm_network_security_rule" "allow_osb_banco_cl_to_kong" {
  name                    = "allow_osb_banco_cl_to_kong"
  priority                = "211"
  direction               = "Inbound"
  access                  = "Allow"
  protocol                = "*"
  source_port_range       = "*"
  destination_port_ranges = [443]

  #Rangos Ip's Ips OSB Banco CL
  source_address_prefixes = [
    "10.233.42.11",
    "10.233.61.130",
    "10.233.61.30",
    "10.233.61.31",
    "10.233.61.32",
    "10.233.61.33",
  ]

  destination_address_prefix  = "*"
  resource_group_name         = "${module.swarm.resource_group_name}"
  network_security_group_name = "${module.swarm.network_security_group_name}"
}

resource "azurerm_network_security_rule" "allow_portabilidad_banco_cl_to_kong" {
  name                    = "allow_portabilidad_banco_cl_to_kong"
  priority                = "212"
  direction               = "Inbound"
  access                  = "Allow"
  protocol                = "*"
  source_port_range       = "*"
  destination_port_ranges = [443, 8443]

  #Rangos Ip's Ips Portabilidad k8s Banco CL
  source_address_prefixes = [
    "10.233.18.0/24",
  ]

  destination_address_prefix  = "*"
  resource_group_name         = "${module.swarm.resource_group_name}"
  network_security_group_name = "${module.swarm.network_security_group_name}"
}
